const userObj = new Object();

userObj.name = "Juan";
userObj.lastName = "Hernandez";
userObj.email = "juan@gmail.com";

const carlos = {
  name: "Carlos",
  lastName: "Valencia",
  email: "carlos02@gmail.com",
};

//Acceder al valor de una propiedad dentro de un objeto
console.log(userObj.email);

//Remplazar el valor de una propiedad dentro de un objeto
carlos.email = "carlos03@gmail.com";

//Agregando una nueva propiedad con su valor
carlos["age"] = 28;
console.log(carlos);

//Listar todas las propiedades de un objeto
console.log(Object.keys(carlos));

//Listar todos los valores de un objeto
console.log(Object.values(carlos));

//Eliminar una propiedad para el objeto carlos
delete carlos.lastName;
console.log(carlos);

//Copiar un objeto

// ... -> spread operator

const copiaCarlos = {...carlos};

console.log(copiaCarlos);

let copiaUserObj = {};

//Realizando una copia de un objeto con el Object.assign
Object.assign(copiaUserObj, userObj);
console.log(copiaUserObj);

const arregloObj = [];

arregloObj.push(carlos);

console.log(arregloObj);